package cz.cvut.eshop.shop;

import cz.cvut.eshop.shop.DiscountedItem;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DiscountedItemTest {

    @Test
    @Ignore
    public void constructorTest() {
        int id = 123456;
        String name = "Zupan";
        float price = 2000;
        String category = "Obleceni";
        int discount = 20;
        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.YEAR, 2018);
        cal1.set(Calendar.MONTH, Calendar.SEPTEMBER);
        cal1.set(Calendar.DAY_OF_MONTH, 1);
        Date discountFrom = cal1.getTime();

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.YEAR, 2018);
        cal2.set(Calendar.MONTH, Calendar.NOVEMBER);
        cal2.set(Calendar.DAY_OF_MONTH, 1);
        Date discountTo = cal2.getTime();

        DiscountedItem discountedItem = new DiscountedItem(id, name, price, category, discount, discountFrom, discountTo);
        Assert.assertNotNull("Created object should not be null", discountedItem);
        Assert.assertEquals("Discounted item's ID should be 123456", discountedItem.getID(), id);
        Assert.assertEquals("Discounted item's name should be Zupan", discountedItem.getName(), name);

        int expectedPrice = Math.round(discountedItem.getPrice());
        int actualPrice = 1600;
        Assert.assertEquals("Discounted item's price does not match", expectedPrice, actualPrice);
        Assert.assertEquals("Discounted item's category should be Obleceni", discountedItem.getCategory(), category);
        Assert.assertEquals("Discounted item's discount should be 20", discountedItem.getDiscount(), discount);
        Assert.assertEquals("Discounted item's discountFrom should be 2018/09/01", discountedItem.getDiscountFrom(), discountFrom);
        Assert.assertEquals("Discounted item's discountTo should be 2018/11/01", discountedItem.getDiscountTo(), discountTo);
    }

    @Test
    public void setDiscountFrom_correctDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2005";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(105, date.getYear());
    }

    @Test
    public void setDiscountFrom_wrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        String newDateString = "abc";

        discountedItem.setDiscountFrom(newDateString);
        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void testSetDiscountToCorrectDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        String newDateString = "15.04.2020";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void testSetDiscountToWrongDateFormatString() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        String newDateString = "abcdefs";

        discountedItem.setDiscountTo(newDateString);
        Date date = discountedItem.getDiscountTo();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(118, date.getYear());
    }

    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceZeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 0, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(1.6f, value, 0.001f);
    }

    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceNozeroDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");

        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.64f, value, 0.001f);
    }

    @Test
    public void testparseDateCorrectDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "abcd", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertNull(date);
    }

    @Test
    public void testparseDateWrongDateFormat() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");

        Date date = discountedItem.getDiscountFrom();

        assertEquals(10, date.getDate());
        assertEquals(4, date.getMonth());
        assertEquals(117, date.getYear());
    }

    @Test
    public void testSetDiscountToCorrectDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        Date newDate = new Date(120, 3, 15);

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertEquals(15, date.getDate());
        assertEquals(3, date.getMonth());
        assertEquals(120, date.getYear());
    }

    @Test
    public void testSetDiscountToNullDateFormatDate() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        Date newDate = null;

        discountedItem.setDiscountTo(newDate);
        Date date = discountedItem.getDiscountTo();

        assertNull(date);
    }


    //TODO: see Ignore
    @Ignore("function getDiscountedPrice() in class DiscountedItem does not divide percents by 100" +
            " so the price result of multiplication is incorrect. " +
            "Possible change replacement of line 114 is: return super.getPrice()*((100 - discount)/100.0f);")
    @Test
    public void testGetDiscountedPriceReflectsChangeOfDiscount() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");
        int newDiscount = 40;

        discountedItem.setDiscount(newDiscount);
        int actual = discountedItem.getDiscount();
        float value = discountedItem.getDiscountedPrice();

        assertEquals(0.96f, value, 0.001f);
        assertEquals(newDiscount, actual);
    }

    @Test
    public void testGetOriginalPricePriceIsCorrect() {
        DiscountedItem discountedItem = new DiscountedItem(1, "name1", 1.6f, "category1", 60, "10.05.2017", "10.05.2018");

        float actual = discountedItem.getOriginalPrice();

        assertEquals(1.6f, actual, 0.001f);
    }
}
